﻿using UnityEngine;
using System.Collections;

public class CountManager : MonoBehaviour {
	public GUIStyle textstyle;
	public float Sw;
	public float Sh;
	public int TotalCount;
	public int CurrentCount;
	public int LoseCount;
	public Rect Gamewindow;
	public bool Gover;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
		CurrentCount = 0;
		LoseCount = 0;
		Gover = false;
	}
	
	// Update is called once per frame
	void Update () {
		if ((CurrentCount + LoseCount) == TotalCount) {
			Gover = true;
			Time.timeScale = 0;
				}
	}

	void OnGUI(){
		GUI.Label (new Rect(ResizeX (550), ResizeY (20), ResizeW (230), ResizeH (50)),"Count: " + CurrentCount + "/" + TotalCount,textstyle);
		if (Gover == true) {
			Gamewindow = GUI.Window (0, new Rect (ResizeX (270), ResizeY (100), ResizeW (230), ResizeH (150)), DoMyWindow, "Game Over");
		}
	}
	
	void DoMyWindow(int windowID) {
		if (GUI.Button (new Rect (ResizeX (85), ResizeY (120), ResizeW (70), ResizeH (20)), "Exit")) {
			Application.LoadLevel("Rank");
		}
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}
}
