﻿using UnityEngine;
using System.Collections;

public class GameTimeManager : MonoBehaviour {
	public int min;
	public int sec;
	public int setmin;
	public int setsec;
	public GUIStyle Timestyle;
	public bool Over;
	public Rect Gamewindow;
	public float Sw;
	public float Sh;
	public ScoreManager Rscore;
	public GameObject scoremanager;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
		Rscore = scoremanager.GetComponent<ScoreManager>();
		Over = false;
	}
	
	// Update is called once per frame
	void Update () {
		min = (int)Time.timeSinceLevelLoad / 60;
		sec = (int)Time.timeSinceLevelLoad % 60;
		if (setmin == min && setsec == sec) {
			Over = true;
			Time.timeScale = 0;
				}
	}

	void OnGUI(){
				GUI.Label (new Rect (ResizeX (20), ResizeY (20), ResizeW (70), ResizeH (20)), "Time: " + min + ":" + sec,Timestyle);
		if (Over == true) {
			Gamewindow = GUI.Window (0, new Rect (ResizeX (270), ResizeY (100), ResizeW (230), ResizeH (150)), DoMyWindow, "Game Over");
				}
		}

	void DoMyWindow(int windowID) {
		if (GUI.Button (new Rect (ResizeX (85), ResizeY (120), ResizeW (70), ResizeH (20)), "Exit")) {
			PlayerPrefs.SetInt("HighScore",Rscore.Score);
			Application.LoadLevel("Rank");
		}
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}
}
