﻿using UnityEngine;
using System.Collections;

public class StartMenuController : MonoBehaviour {
	public GUIStyle surestyle;
	public GUIStyle resetstyle;
	public GUIStyle textstyle;
	public string nametext;
	public float Sw;
	public float Sh;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.Label (new Rect(ResizeX (270), ResizeY (30), ResizeW (230), ResizeH (50)),"Please import your name.",textstyle);

		nametext = GUI.TextArea (new Rect(ResizeX (270), ResizeY (70), ResizeW (230), ResizeH (50)),nametext,100);

		if (GUI.Button (new Rect (ResizeX (270), ResizeY (150), ResizeW (100), ResizeH (50)), "Sure")) {
			PlayerPrefs.SetString("name",nametext);
			Application.LoadLevel("Character");
				}

		if (GUI.Button (new Rect (ResizeX (400), ResizeY (150), ResizeW (100), ResizeH (50)), "Reset")) {
			nametext = "";
		}
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}

	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}

	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}

	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}

}

