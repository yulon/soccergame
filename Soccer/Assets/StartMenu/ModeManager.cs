﻿using UnityEngine;
using System.Collections;

public class ModeManager : MonoBehaviour {
	public GameObject[] Controllers;
	public int Mode;

	// Use this for initialization
	void Start () {
		Mode = PlayerPrefs.GetInt ("Mode");
	}
	
	// Update is called once per frame
	void Update () {
		switch(Mode){
		case 1:
			Controllers[0].SetActive(true);
			Controllers[1].SetActive(true);
			Controllers[2].SetActive(false);
			break;
		case 2:
			Controllers[0].SetActive(false);
			Controllers[1].SetActive(false);
			Controllers[2].SetActive(true);
			break;
		}
	}
}
