﻿using UnityEngine;
using System.Collections;

public class ScoreManager: MonoBehaviour {
	public GUIStyle textstyle;
	public int Score;
	public float Sw;
	public float Sh;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
		Score = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.Label (new Rect(ResizeX (600), ResizeY (20), ResizeW (230), ResizeH (50)),"Score: " + Score ,textstyle);
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}
}
