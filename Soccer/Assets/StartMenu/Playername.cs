﻿using UnityEngine;
using System.Collections;

public class Playername : MonoBehaviour {
	public GUIStyle shownamesize;
	public string showname;
	public float sw;
	public float sh;

	// Use this for initialization
	void Start () {
		sw = Screen.width;
		sh = Screen.height;
		showname = PlayerPrefs.GetString ("name");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.Label (new Rect(ResizeX (50), ResizeY (30), ResizeW (230), ResizeH (50)),"Name " + " : " + showname,shownamesize);
		}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * sh);
		return Rh;
	}
}
