﻿using UnityEngine;
using System.Collections;

public class BallManager : MonoBehaviour {
	public float Bposy;
	public float Bposx;
	public float Ballspeed;
	public float Ballspeed2;
	public bool up;
	public bool down;
	public bool right;
	public bool left;
	public bool open1;
	public int bcondition;
	public int fcondition;

	public ScoreManager Addscore;
	public GameObject Setscore;

	public CountManager Addcount;
	public GameObject Setcount;
	
	// Use this for initialization
	void Start () {
		Addscore = Setscore.GetComponent<ScoreManager>();
		Addcount = Setcount.GetComponent<CountManager>();
		bcondition = PlayerPrefs.GetInt ("Bcondition");
		fcondition = PlayerPrefs.GetInt ("Footway");
		up = false;
		down = true;
		right = false;
		left = false;
		open1 = false;
		Setballposition (fcondition);
	}
	
	// Update is called once per frame
	void Update () {
		switch(bcondition){

		case 1:
			if (up == true) {
				this.gameObject.transform.Translate (0.0f, Ballspeed * Time.deltaTime, 0.0f);
					} else {
				this.gameObject.transform.Translate (0.0f, -Ballspeed * Time.deltaTime, 0.0f);
					}
			if (this.gameObject.transform.position.y >= Bposy) {
				up = false;
					}
			if (this.gameObject.transform.position.y < -5.0f) {
				transform.position = new Vector3(transform.position.x,Bposy,transform.position.z);
				Addcount.LoseCount += 1;
					}
			break;

		case 2:
			if (up == true) {
				this.gameObject.transform.Translate (0.0f, Ballspeed * Time.deltaTime, 0.0f);
			} 
			else if(down == true){
				this.gameObject.transform.Translate (0.0f, -Ballspeed * Time.deltaTime, 0.0f);
			}
			else if (right == true) {
				this.gameObject.transform.Translate (Ballspeed * Time.deltaTime, 0.0f, 0.0f);
			} 
			else if(left == true){
				this.gameObject.transform.Translate (-Ballspeed * Time.deltaTime, 0.0f, 0.0f);
			}

			if (this.gameObject.transform.position.y >= Bposy) {
				this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x,Bposy,this.gameObject.transform.position.z);
			}
			if(open1 == false){
				if(this.gameObject.transform.position.x <= -1.0f && this.gameObject.transform.position.y == Bposy){
					up = false; right = true; left = false; down = false;
				}

				else if(this.gameObject.transform.position.x >= 1.0f && this.gameObject.transform.position.y == Bposy){
					up = false; right = false; left = false; down = true; open1 = true;
				}
			}
			else{
				if(this.gameObject.transform.position.x >= 1.0f && this.gameObject.transform.position.y == Bposy){
					up = false; right = false; left = true; down = false;
				}
				
				else if(this.gameObject.transform.position.x <= -1.0f && this.gameObject.transform.position.y == Bposy){
					up = false; right = false; left = false; down = true; open1 = false;
				}
			}

			if (this.gameObject.transform.position.y < -5.0f) {
				transform.position = new Vector3(transform.position.x,Bposy,transform.position.z);
				Addcount.LoseCount += 1;
			}

			break;
		}
	}

	void Setballposition(int BallMode){
		switch(BallMode){
		case 1:
			this.gameObject.transform.position = new Vector3(-1.0f,8.0f,5.0f);
			break;
		case 2:
			this.gameObject.transform.position = new Vector3(-0.3f,8.0f,3.5f);
			break;
		case 3:
			this.gameObject.transform.position = new Vector3(-1.0f,8.0f,5.0f);
			break;
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "colliderpoint") {
			up = true;
			down = false;
			Addscore.Score += 10;
			Addcount.CurrentCount += 1;
				}
	}

	void OnTriggerStay(Collider other){
		if (other.gameObject.tag == "colliderpoint") {
			up = true;
			down = false;
		}
	}
}
