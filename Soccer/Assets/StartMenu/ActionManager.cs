﻿using UnityEngine;
using System.Collections;

public class ActionManager : MonoBehaviour {
	public Animator Action;
	public int Footways;

	// Use this for initialization
	void Start () {
		Action = GetComponent<Animator>();
		Footways = PlayerPrefs.GetInt ("Footway");
	}
	
	// Update is called once per frame
	void Update () {

		switch(Footways){

		case 1:
			if (Input.GetKeyDown (KeyCode.A)) {
				Action.SetTrigger("Knee");
				Action.SetBool("Idle",false);
			}
			break;

		case 2:
			if (Input.GetKeyDown (KeyCode.A)) {
				Action.SetTrigger("Kick");
				Action.SetBool("Idle",false);
				this.gameObject.transform.eulerAngles = new Vector3(0.0f,180.0f,0.0f);
			}
			break;

		case 3:
			if (Input.GetKeyDown (KeyCode.A)) {
				Action.SetTrigger("Knee");
				Action.SetBool("Idle",false);
			}
			if (Input.GetKeyDown (KeyCode.D)) {
				Action.SetTrigger("Alter");
				Action.SetBool("Idle",false);
			}
			break;
		}

		if(Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.D)){
			Action.SetBool("Idle",true);
		}

	}
}
