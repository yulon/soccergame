﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public GameObject[] Character;
	public int[] Cnum;

	// Use this for initialization
	void Start () {
		for (int a = 0; a < 4; a ++) {
			Character[a].SetActive(false);
				}
		Cnum [0] = PlayerPrefs.GetInt ("C1");
		Cnum [1] = PlayerPrefs.GetInt ("C2");
		Cnum [2] = PlayerPrefs.GetInt ("C3");
		Cnum [3] = PlayerPrefs.GetInt ("C4");
	}
	
	// Update is called once per frame
	void Update () {
		for (int b = 0; b < 4; b++) {
			if(Cnum[b] == 1){
				Character[b].SetActive(true);
			}
				}
	}
}
