﻿using UnityEngine;
using System.Collections;

public class Choosefoot : MonoBehaviour {
	public GUIStyle textstyle;
	public float Sw;
	public float Sh;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.Label (new Rect(ResizeX (270), ResizeY (70), ResizeW (230), ResizeH (50)),"Choose Foot.",textstyle);
		
		if (GUI.Button (new Rect (ResizeX (330), ResizeY (150), ResizeW (100), ResizeH (50)), "Single")) {
			PlayerPrefs.SetInt("Footway",1);
			PlayerPrefs.SetInt("Bcondition",1);
			Application.LoadLevel("Importname");
		}
		
		if (GUI.Button (new Rect (ResizeX (330), ResizeY (220), ResizeW (100), ResizeH (50)), "Instep")) {
			PlayerPrefs.SetInt("Footway",2);
			PlayerPrefs.SetInt("Bcondition",1);
			Application.LoadLevel("Importname");
		}

		if (GUI.Button (new Rect (ResizeX (330), ResizeY (290), ResizeW (100), ResizeH (50)), "Alternation")) {
			PlayerPrefs.SetInt("Footway",3);
			PlayerPrefs.SetInt("Bcondition",2);
			Application.LoadLevel("Importname");
		}
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}
}
