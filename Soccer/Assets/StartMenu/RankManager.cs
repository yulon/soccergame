﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class RankManager : MonoBehaviour {
	public int[] ScoreList = new int[6];
	public int Cscore = 0;
	public int Bscore = 0;
	public int Ascore = 0;
	public int a;
	public GUIStyle EndScoreStyle;
	public float Sw;
	public float Sh;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
		//Rank (PlayerPrefs.GetInt("HighScore"));
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Space)) {
						Rank (a);
				}
	}

	void Rank(int CurrentScore){

		if (CurrentScore > Bscore) {
						for (int i =  ScoreList.Length - 1; i > 0; i--) {
								ScoreList [i] = CurrentScore;
								if (ScoreList [i] > ScoreList [i - 1]) {
										Ascore = ScoreList [i - 1];
										ScoreList [i - 1] = ScoreList [i];
										ScoreList [i] = Ascore;
								}
						} 
						Bscore = CurrentScore;
				} else {
						for (int i =  1; i < ScoreList.Length - 1; i++) {
								Cscore = CurrentScore;
								if (Cscore > ScoreList [i]) {
										Ascore = ScoreList [i];
										ScoreList [i] = Cscore;
										ScoreList [i + 1] = Ascore;
										break;
								}
						} 
				}
		//for (int i = 0; i < ScoreList.Length; i++) {
			try{
				StreamWriter sw = new StreamWriter("C:\\Test.txt");
				sw.WriteLine(ScoreList[0]);
				sw.WriteLine("\t");
				sw.WriteLine(ScoreList[1]);
				sw.WriteLine("\t");
				sw.WriteLine(ScoreList[2]);
				sw.WriteLine("\t");
				sw.WriteLine(ScoreList[3]);
				sw.WriteLine("\t");
				sw.WriteLine(ScoreList[4]);
				sw.WriteLine("\t");
				sw.WriteLine(ScoreList[5]);
				sw.WriteLine("\t");
				//sw.WriteLine("Hello HHHHHH!!");
				sw.Close();
			}
			catch(Exception e){
				Console.WriteLine("Exception: " + e.Message);
			}
			finally{
				Console.WriteLine("Executing finally block.");
			}
				//}
			}

	void OnGUI(){
		/*GUI.Label(new Rect(ResizeX(350), ResizeY(50), Screen.width/2, Screen.height/2-100),"1: "+HighScore[5],EndScoreStyle);
		GUI.Label(new Rect(ResizeX(350), ResizeY(100), Screen.width/2, Screen.height/2+100),"2: "+HighScore[4] ,EndScoreStyle);
		GUI.Label(new Rect(ResizeX(350), ResizeY(150), Screen.width/2, Screen.height/2+300),"3: "+HighScore[3],EndScoreStyle);
		GUI.Label(new Rect(ResizeX(350), ResizeY(200), Screen.width/2, Screen.height/2+500),"4: "+HighScore[2] ,EndScoreStyle);
		GUI.Label(new Rect(ResizeX(350), ResizeY(250), Screen.width/2, Screen.height/2+700),"5: "+HighScore[1] ,EndScoreStyle);
		GUI.Label(new Rect(ResizeX(350), ResizeY(300), Screen.width/2, Screen.height/2+900),"6: "+HighScore[0],EndScoreStyle);*/
		
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}
}
