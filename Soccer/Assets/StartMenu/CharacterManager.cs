﻿using UnityEngine;
using System.Collections;

public class CharacterManager : MonoBehaviour {
	public GameObject[] PID;
	public Bechoosed[] ID;
	public Rect windowRect;
	public float Sw;
	public float Sh;

	// Use this for initialization
	void Start () {
		Sw = Screen.width;
		Sh = Screen.height;
		for (int a = 0; a < 4; a ++) {
			ID[a] = PID[a].GetComponent<Bechoosed>();
				}
		PlayerPrefs.SetInt("C1",0);
		PlayerPrefs.SetInt("C2",0);
		PlayerPrefs.SetInt("C3",0);
		PlayerPrefs.SetInt("C4",0);
	}
	
	// Update is called once per frame
	void Update () {

		if (ID [0].choosed == true) {
			PlayerPrefs.SetInt("C1",1);
				}
		else if (ID [1].choosed == true) {
			PlayerPrefs.SetInt("C2",1);
		}
		else if (ID [2].choosed == true) {
			PlayerPrefs.SetInt("C3",1);
		}
		else if (ID [3].choosed == true) {
			PlayerPrefs.SetInt("C4",1);
		}


	}

	void OnGUI(){
		for (int b = 0; b < 4; b ++) {
						if (ID[b].choosed == true) {
								windowRect = GUI.Window (0, new Rect (ResizeX (270), ResizeY (70), ResizeW (230), ResizeH (50)), DoMyWindow, "Are you sure?");
						}
				}
	}

	void DoMyWindow(int windowID) {
		if (GUI.Button (new Rect (ResizeX (20), ResizeY (20), ResizeW (70), ResizeH (20)), "Sure")) {
			Application.LoadLevel("Game");
				}

		if (GUI.Button (new Rect (ResizeX (140), ResizeY (20), ResizeW (70), ResizeH (20)), "Cancel")) {
						for (int c = 0; c < 4; c++) {
								if (ID [c].choosed == true) {
										ID [c].choosed = false;
								}
						}
				}
	}

	public int ResizeX(int x){
		int Rx = 0;
		Rx = (int)((x / 743.0f) * Sw);
		return Rx;
	}
	
	public int ResizeY(int y){
		int Ry = 0;
		Ry = (int)((y / 343.0f) * Sh);
		return Ry;
	}
	
	public int ResizeW(int w){
		int Rw = 0;
		Rw = (int)((w / 743.0f) * Sw);
		return Rw;
	}
	
	public int ResizeH(int h){
		int Rh = 0;
		Rh = (int)((h / 343.0f) * Sh);
		return Rh;
	}

}
